# VUE.js docker environment

## Building and Running

### In development environment

```./run```

Starts up the local yarn serve / npm serve which will autoreload the app when changes appear.

### In production environment

```./run-prod```

Boots up the docker setup using 2 containers:
- build-stage will be used to package the application with node
- production-stage uses the build result to serve via nginx

This setup does not react to source changes. You need to rebuild to deploy.

## Vue.s infos

### Project setup
```
yarn install
```

#### Compiles and hot-reloads for development
```
yarn serve
```

#### Compiles and minifies for production
```
yarn build
```

#### Lints and fixes files
```
yarn lint
```

#### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
